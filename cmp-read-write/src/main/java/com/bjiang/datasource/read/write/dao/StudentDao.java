package com.bjiang.datasource.read.write.dao;

import com.bjiang.datasource.read.write.entity.Student;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentDao {
    List<Student> getStudent();
    Integer insertStudent(Student student);
}
