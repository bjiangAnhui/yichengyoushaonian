package com.bjiang.datasource.read.write.config;

/**
 * @author bjiang
 * @version 1.0
 * @date 2020/6/22 9:51
 */
public class DataSourceThread {
    private static final ThreadLocal<String> DATASOURCE_THREAD = new ThreadLocal<>();

    public static void set() {
        DATASOURCE_THREAD.set("slave");
    }

    public static String get() {
        return DATASOURCE_THREAD.get();
    }

    public static void clear() {
        DATASOURCE_THREAD.remove();
    }
}
