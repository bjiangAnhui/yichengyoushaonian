package com.bjiang.datasource.read.write.controller;

import com.bjiang.datasource.read.write.aop.Slave;
import com.bjiang.datasource.read.write.entity.Student;
import com.bjiang.datasource.read.write.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author bjiang
 * @version 1.0
 * @date 2020/6/22 13:35
 */
@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @RequestMapping("/getStudent")

    @Slave
    public List<Student> getStudent() {
        return studentService.getStudent();
    }
    @RequestMapping("/insertStudent")
    public Integer  insertStudent() {
        Student student=new Student();
        student.setName("name");
        student.setSex("男");
        return studentService.insertStudent(student);
    }
}
