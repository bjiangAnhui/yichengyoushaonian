package com.bjiang.datasource.read.write.aop;

import com.bjiang.datasource.read.write.config.DataSourceThread;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
/**
 * @author bjiang
 * @version 1.0
 * @date 2020/6/22 10:00
 */
@Slf4j
@Aspect
@Order(value = 1)
@Component
public class DataSourceAop {
    @Around("@annotation(com.bjiang.datasource.read.write.aop.Slave)")
    public Object setDynamicDataSource(ProceedingJoinPoint pjp) throws Throwable {
        boolean clear = true;
        try {
            DataSourceThread.set();
            log.info("切换数据源");
            return pjp.proceed();
        } finally {
            if (clear) {
                DataSourceThread.clear();
            }
        }
    }
}
