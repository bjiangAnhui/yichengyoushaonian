package com.bjiang.datasource.read.write.entity;

import lombok.Data;

/**
 * @author bjiang
 * @version 1.0
 * @date 2020/6/22 13:27
 */
@Data
public class Student {
    private String name;
    private String sex;
}
