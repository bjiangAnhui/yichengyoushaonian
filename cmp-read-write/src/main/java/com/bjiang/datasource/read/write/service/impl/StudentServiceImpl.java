package com.bjiang.datasource.read.write.service.impl;

import com.bjiang.datasource.read.write.dao.StudentDao;
import com.bjiang.datasource.read.write.entity.Student;
import com.bjiang.datasource.read.write.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author bjiang
 * @version 1.0
 * @date 2020/6/22 13:33
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;
    @Override
    public List<Student> getStudent() {
        return studentDao.getStudent();
    }

    @Override
    public Integer insertStudent(Student student) {
        return studentDao.insertStudent(student);
    }
}
