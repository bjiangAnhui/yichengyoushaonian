package com.bjiang.datasource.read.write;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class CmpReadWriteApplication {

    public static void main(String[] args) {
        SpringApplication.run(CmpReadWriteApplication.class, args);
    }

}
