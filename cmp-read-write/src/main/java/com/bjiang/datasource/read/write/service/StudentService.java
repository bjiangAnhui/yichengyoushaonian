package com.bjiang.datasource.read.write.service;

import com.bjiang.datasource.read.write.entity.Student;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface StudentService {
    List<Student> getStudent();
    Integer insertStudent(Student student);
}
