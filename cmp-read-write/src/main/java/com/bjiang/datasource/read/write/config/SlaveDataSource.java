package com.bjiang.datasource.read.write.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author bjiang
 * @version 1.0
 * @date 2020/6/22 9:59
 * 获取datasource的逻辑
 */
public class SlaveDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceThread.get();
    }
}
