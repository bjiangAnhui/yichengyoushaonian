package com.bjiang.datasource.read.write.aop;


import java.lang.annotation.*;

/**
 * 自定义注解实现AOP,用于注解需要使用从库的方法
 * */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Slave {
}
